/* *****************************************************************************
 * Project name: Dictionary
 * File name   : tests
 * Author      : Damien Nguyen
 * Date        : Friday, April 10, 2020
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "dictionary.h"
#include "entry.h"
#include "tree.h"
#include "tests.h"

static void t_print_root(void *data, int position) {
    for (int i = 0; i < position; ++i) {
        printf("%16s", "");
    }

    printf("%s\n", (char *) data);
}

void entry_tests(const char *file_path) {
    entry_t *entries[100], *entry = NULL, *input = NULL;
    FILE    *file                 = NULL;
    int     i                     = 0, j;

    if (!(file = fopen(file_path, FILE_READ))) {
        perror("fopen");
        exit(errno);
    }

    while (!feof(file) && (entry = e_read(file))) {
        entries[i++] = entry;
    }

    fclose(file);

    for (j = 0; j < i; ++j) {
        printf("%2d: %s\n", j + 1, entries[j]->word);
    }

    for (j = 0; j < i; ++j) {
        e_free(entries[j]);
    }

    input = d_input_new_entry();
    printf("Entry:\n\tWord: %s\n\tDefinition: %s\n", input->word, input->def);
    e_free(input);
}

void str_tree_tests(const char *file_path) {
    tree_t tree = NULL;
    char   tmp[WORD_LEN];

    strcpy(tmp, file_path);
    tree = t_add_sorted(tree, "test4", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test1", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test3", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test2", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test5", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test7", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test6", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test9", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, "test8", (cmp_fn) strcmp);
    tree = t_add_sorted(tree, tmp, (cmp_fn) strcmp);

    for (unsigned int i = 0; i < 3; ++i) {
        tree = t_del_max(&tree, (cmp_fn) strcmp, NULL);
    }

    tree = t_del(&tree, "test5", (cmp_fn) strcmp, NULL, NULL);

    t_print(tree, 0, t_print_root);
    t_free(&tree, (cmp_fn) strcmp, NULL);
}
