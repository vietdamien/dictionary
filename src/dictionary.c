/* *****************************************************************************
 * Project name: Dictionary
 * File name   : dictionary
 * Author      : Damien Nguyen
 * Date        : Friday, April 10, 2020
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "dictionary.h"
#include "io.h"

static void (*actions[])() = {
        NULL,
        d_print_tree,
        d_print_list,
        d_add,
        d_del,
        d_find,
        d_count,
        d_check_sort,
        d_clear,
        d_save,
};

static void t_print_root(void *data, int position) {
    for (int i = 0; i < position; ++i) {
        printf("%16s", "");
    }

    printf("%s\n", ((entry_t *) data)->word);
}

static void t_write(tree_t tree, FILE *file) {
    if (!t_is_empty(tree)) {
        entry_t *e = tree->data;
        t_write(t_left(tree), file);
        fprintf(file, "%s\n", e->word);
        if (file != stdout) {
            fprintf(file, "%s\n", e->def);
        }
        t_write(t_right(tree), file);
    }
}

bool_t d_handle(tree_t *tree, unsigned int choice) {
    bool_t doable;
    size_t array_size = sizeof(actions) / sizeof(*actions);

    if ((doable = choice < array_size && *actions[choice])) {
        (*actions[choice])(tree);
        printf("Press enter to continue...\n");
        scanf("%*c");
    }

    return doable;
}

unsigned int d_menu(void) {
    printf(CLEAR_SCREEN);
    print_header("MENU");
    printf("1 - Print dictionary as tree.\n"
           "2 - Print dictionary as list.\n"
           "3 - Add entry.\n"
           "4 - Delete entry.\n"
           "5 - Find a word.\n"
           "6 - Count words.\n"
           "7 - Check if sorted.\n"
           "8 - Clear dictionary.\n"
           "9 - Save dictionary.\n"
           "* - Quit\n\n");

    return (unsigned int) input_number("Choice: ");
}

entry_t *d_input_entry(void) {
    void (*func)() = d_print_tree;
    entry_t *result = NULL;

    while (e_has_empty_word(result)) {
        e_free(result);

        result = alloc(sizeof(entry_t));

        result->word = input_line("Word: ");
        result->def  = alloc(sizeof(char));
        strcpy(result->def, EMPTY_STR);
    }

    return result;
}

entry_t *d_input_new_entry(void) {
    entry_t *result = NULL;

    while (e_has_empty_fields(result)) {
        e_free(result);

        result = alloc(sizeof(entry_t));

        result->word = input_line("Word: ");
        result->def  = input_line("Definition: ");
    }

    return result;
}

tree_t d_load(const char *file_path) {
    FILE *file = NULL;

    entry_t *entry = NULL;
    tree_t  tree   = NULL;

    if (!(file = fopen(file_path, FILE_READ))) {
        perror("d_load");
        exit(errno);
    }

    while (!feof(file) && (entry = e_read(file))) {
        tree = t_add_sorted(tree, entry, (cmp_fn) e_cmp);
    }

    fclose(file);
    return tree;
}

void d_print_tree(tree_t *tree) {
    t_print(*tree, 0, t_print_root);
}

void d_print_list(tree_t *tree) {
    t_write(*tree, stdout);
}

void d_add(tree_t *tree) {
    entry_t *input = d_input_new_entry();
    *tree = t_add_sorted(*tree, input, (cmp_fn) e_cmp);
}

void d_del(tree_t *tree) {
    entry_t *input = d_input_entry();
    if (t_contains(*tree, input, (cmp_fn) e_cmp)) {
        *tree = t_del(tree, input, (cmp_fn) e_cmp, (free_fn) e_free,
                      (cp_fn) e_cp);
    } else {
        fprintf(stderr, "No matching entry.\n");
    }

    e_free(input);
}

void d_find(tree_t *tree) {
    cell_t  *result;
    entry_t *input = d_input_entry();

    if ((result = t_find(*tree, input, (cmp_fn) e_cmp))) {
        entry_t *e = (entry_t *) result->data;
        printf("Definition for %s:\n\t%s\n", e->word, e->def);
    } else {
        fprintf(stderr, "No matching entry.\n");
    }

    e_free(input);
}

void d_count(tree_t *tree) {
    int c = t_node_count(*tree);
    printf("\n%d saved word%s.\n\n", c, c < 2 ? "" : "s");
}

void d_check_sort(tree_t *tree) {
    printf("\nDictionary %ssorted.\n\n",
           t_is_sorted(*tree, (cmp_fn) e_cmp) ? "" : "not ");
}

void d_clear(tree_t *tree) {
    t_free(tree, (cmp_fn) e_cmp, (free_fn) e_free);
}

void d_save(tree_t *tree) {
    char *file_path = input_line("Path: ");
    FILE *file      = NULL;

    if (!(file = fopen(file_path, FILE_WRITE))) {
        perror("d_save");
        exit(errno);
    }

    if (!t_is_empty(*tree)) {
        t_write(*tree, file);
    }

    fclose(file);
    free(file_path);
}

void run(const char *file_path) {
    tree_t tree = d_load(file_path);

    while (d_handle(&tree, d_menu()));

    t_free(&tree, (cmp_fn) e_cmp, (free_fn) e_free);
}
