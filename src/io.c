/* *****************************************************************************
 * Project name: Dictionary
 * File name   : io
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "io.h"
#include "util.h"

static void print_lines(size_t len) {
    size_t i;

    for (i = 0; i < 3 * len; ++i) {
        printf("=");
    }
}

char *input_line(const char *prompt_msg) {
    char *str = NULL, tmp[WORD_LEN];

    printf("%s", prompt_msg);
    fflush(stdout);

    str = alloc(read_line(tmp, WORD_LEN, stdin) * sizeof(char));
    strcpy(str, tmp);

    return str;
}

double input_number(const char *prompt_msg) {
    char   *tmp = NULL, *end_ptr = NULL;
    double number;

    tmp    = input_line(prompt_msg);
    number = strtod(tmp, &end_ptr);

    if (!number && tmp == end_ptr) {
        fprintf(stderr, "read_number: read failed, setting 0 as value.\n");
        number = 0;
    }

    free(tmp);
    return number;
}

size_t read_line(char *dst, size_t len, FILE *file) {
    size_t read = 0;

    if (fgets(dst, len, file) && dst[(read = strlen(dst)) - 1] == CHAR_EOL) {
        dst[read - 1] = CHAR_EOW;
    }

    return read;
}

void print_header(const char *title) {
    size_t len               = strlen(title);
    char   title_f[WORD_LEN] = "\n%", title_end[WORD_LEN];

    sprintf(title_end, "%lds", len);
    strcat(title_f, title_end);
    strcat(title_f, "%s\n");

    printf("\n");
    print_lines(len);
    printf(title_f, "", title);
    print_lines(len);
    printf("\n");
}
