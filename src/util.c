/* *****************************************************************************
 * Project name: Dictionary
 * File name   : util
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

bool_t str_is_empty(const char *str) {
    return !strcmp(str, EMPTY_STR);
}

void *alloc(size_t size) {
    void *data = NULL;

    if (!(data = malloc(size))) {
        perror("alloc");
        exit(errno);
    }

    return data;
}
