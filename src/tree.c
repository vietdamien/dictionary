/* *****************************************************************************
 * Project name: Dictionary
 * File name   : tree
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "tree.h"

int max(int x, int y) {
    return x >= y ? x : y;
}

bool_t t_contains(tree_t tree, void *data, cmp_fn cfn) {
    int cmp;

    if (t_is_empty(tree)) {
        return false;
    }
    if (!(cmp = (*cfn)(data, t_root(tree)))) {
        return true;
    }

    return cmp < 0 ? t_contains(t_left(tree), data, cfn)
                   : t_contains(t_right(tree), data, cfn);
}

bool_t t_equal(tree_t l, tree_t r, cmp_fn cfn) {
    if (t_is_empty(l) && t_is_empty(r)) {
        return true;
    }

    if (t_is_empty(l) || t_is_empty(r)) {
        return false;
    }

    return (*cfn)(t_root(l), t_root(r)) == 0
           && t_equal(t_left(l), t_left(r), cfn)
           && t_equal(t_right(l), t_right(r), cfn);
}

bool_t t_is_empty(tree_t tree) {
    return !tree;
}

bool_t t_is_leaf(tree_t tree) {
    return !t_is_empty(tree)
           && t_is_empty(t_left(tree)) && t_is_empty(t_right(tree));
}

bool_t t_is_sorted(tree_t tree, cmp_fn cfn) {
    bool_t right, left, rec;

    if (t_is_empty(tree) || t_is_leaf(tree)) {
        return true;
    }

    right = (right = t_is_empty(t_left(tree)))
            ? right
            : (*cfn)(t_root(t_right_end(t_left(tree))), t_root(tree)) < 0;
    left  = (left  = t_is_empty(t_right(tree)))
            ? left
            : (*cfn)(t_root(t_left_end(t_right(tree))), t_root(tree)) > 0;
    rec   = t_is_sorted(t_left(tree), cfn) && t_is_sorted(t_right(tree), cfn);

    return right && left && rec;
}

cell_t *t_find(tree_t tree, void *data, cmp_fn cfn) {
    int cmp;

    if (t_is_empty(tree)) {
        return NULL;
    }

    if ((cmp = (*cfn)(data, t_root(tree))) == 0) {
        return tree;
    }

    return cmp < 0 ? t_find(t_left(tree), data, cfn)
                   : t_find(t_right(tree), data, cfn);
}

couple_t t_cut(tree_t tree, void *data, cmp_fn cfn) {
    couple_t c, tmp;

    if (t_is_empty(tree)) {
        c.l = c.r = NULL;
    } else {
        int cmp = (*cfn)(data, t_root(tree));
        if (cmp == 0) {
            c.l = t_left(tree);
            c.r = t_right(tree);
        } else if (cmp < 0) {
            tmp = t_cut(t_left(tree), data, cfn);
            c.l = tmp.l;
            c.r = t_embed_cell(tmp.r, tree, t_right(tree));
        } else {
            tmp = t_cut(t_right(tree), data, cfn);
            c.r = tmp.r;
            c.l = t_embed_cell(t_left(tree), tree, tmp.l);
        }
    }

    return c;
}

int t_diff(tree_t tree) {
    return t_is_empty(tree)
           ? 0
           : t_height(t_left(tree)) - t_height(t_right(tree));
}

int t_height(tree_t tree) {
    return t_is_empty(tree)
           ? EMPTY_TREE
           : max(t_height(t_left(tree)), t_height(t_right(tree))) + 1;
}

int t_leaf_count(tree_t tree) {
    if (t_is_empty(tree)) {
        return 0;
    }
    if (t_is_leaf(tree)) {
        return 1;
    }
    return t_leaf_count(t_left(tree)) + t_leaf_count(t_right(tree));
}

int t_node_count(tree_t tree) {
    if (t_is_empty(tree)) {
        return 0;
    }
    return t_node_count(t_left(tree)) + t_node_count(t_right(tree)) + 1;
}

tree_t t_add_root(tree_t tree, void *data, cmp_fn cfn) {
    couple_t c = t_cut(tree, data, cfn);
    return t_embed(c.l, data, c.r);
}

tree_t t_add_sorted(tree_t tree, void *data, cmp_fn cfn) {
    int cmp;

    if (t_is_empty(tree)) {
        return t_add_root(tree, data, cfn);
    }

    if ((cmp = (*cfn)(data, t_root(tree))) == 0) {
        return tree;
    }

    if (cmp < 0) {
        tree->l = t_add_sorted(tree->l, data, cfn);
    } else {
        tree->r = t_add_sorted(tree->r, data, cfn);
    }

    return t_balance(tree);
}

tree_t t_embed(tree_t l, void *data, tree_t r) {
    cell_t *new = alloc(sizeof(cell_t));

    new->data = data;
    return t_embed_cell(l, new, r);
}

tree_t t_embed_cell(tree_t l, cell_t *cell, tree_t r) {
    cell->l = l;
    cell->r = r;
    return cell;
}

tree_t t_del(tree_t *tree, void *data, cmp_fn cfn, free_fn ffn, cp_fn cp) {
    int cmp;

    if (t_is_empty(*tree)) {
        return *tree;
    }

    if ((cmp = (*cfn)(data, t_root(*tree))) == 0) {
        return t_del_root(tree, cfn, ffn, cp);
    }

    if (cmp < 0) {
        (*tree)->l = t_del(&(*tree)->l, data, cfn, ffn, cp);
    } else {
        (*tree)->r = t_del(&(*tree)->r, data, cfn, ffn, cp);
    }

    return t_balance(*tree);
}

tree_t t_del_max(tree_t *tree, cmp_fn cfn, free_fn ffn) {
    if (t_is_empty(*tree)) {
        return *tree;
    }
    if (t_is_leaf(*tree)) {
        t_free_cell(*tree, ffn);
        return NULL;
    }
    if (t_is_empty(t_right(*tree))) {
        tree_t t = t_left(*tree);
        t_free_cell(*tree, ffn);
        return t_balance(t);
    }
    (*tree)->r = t_del_max(&(*tree)->r, cfn, ffn);

    return t_balance(*tree);
}

tree_t t_del_root(tree_t *tree, cmp_fn cfn, free_fn ffn, cp_fn cp) {
    cell_t *l_max;
    tree_t l, r = t_right(*tree);
    void   *data;

    if (t_is_empty(t_left(*tree))) {
        t_free_cell(*tree, ffn);
        return t_balance(r);
    }

    l_max = t_right_end(t_left(*tree));
    data  = (*ffn) ? (*cp)(l_max->data) : l_max->data;
    l     = t_del_max(&(*tree)->l, cfn, ffn);

    t_free_cell(*tree, ffn);
    return t_balance(t_embed(l, data, r));
}

tree_t t_left(tree_t tree) {
    if (t_is_empty(tree)) {
        fprintf(stderr, "t_left: empty tree.\n");
        exit(EXIT_FAILURE);
    }

    return tree->l;
}

tree_t t_left_end(tree_t tree) {
    if (t_is_empty(tree)) {
        fprintf(stderr, "t_left_end: empty tree.\n");
        exit(EXIT_FAILURE);
    }

    return t_is_empty(t_left(tree))
           ? t_embed_cell(NULL, tree, t_right(tree))
           : t_left_end(t_left(tree));
}

tree_t t_right(tree_t tree) {
    if (t_is_empty(tree)) {
        fprintf(stderr, "t_right: empty tree.\n");
        exit(EXIT_FAILURE);
    }

    return tree->r;
}

tree_t t_right_end(tree_t tree) {
    if (t_is_empty(tree)) {
        fprintf(stderr, "t_right_end: empty tree.\n");
        exit(EXIT_FAILURE);
    }

    return t_is_empty(t_right(tree))
           ? t_embed_cell(t_left(tree), tree, NULL)
           : t_right_end(t_right(tree));
}

tree_t rotate_l(tree_t tree) {
    tree_t right;

    if (t_is_empty(tree)) {
        return tree;
    }

    if (t_is_empty(right = t_right(tree))) {
        fprintf(stderr, "rotate_l: empty right tree.\n");
        exit(EXIT_FAILURE);
    }

    return t_embed_cell(
            t_embed_cell(t_left(tree), tree, t_left(right)),
            right,
            t_right(right)
    );
}

tree_t rotate_lr(tree_t tree) {
    if (t_is_empty(tree) || t_is_empty(t_left(tree))) {
        return tree;
    }

    if (t_is_empty(t_right(t_left(tree)))) {
        fprintf(stderr, "rotate_lr: empty right tree within left tree.\n");
        exit(EXIT_FAILURE);
    }

    return rotate_r(
            t_embed_cell(rotate_l(t_left(tree)), tree, t_right(tree))
    );
}

tree_t rotate_r(tree_t tree) {
    tree_t left;

    if (t_is_empty(tree)) {
        return tree;
    }

    if (t_is_empty(left = t_left(tree))) {
        fprintf(stderr, "rotate_r: empty left tree.\n");
        exit(EXIT_FAILURE);
    }

    return t_embed_cell(
            t_left(left), left, t_embed_cell(t_right(left), tree, t_right(tree))
    );
}

tree_t rotate_rl(tree_t tree) {
    if (t_is_empty(tree) || t_is_empty(t_right(tree))) {
        return tree;
    }

    if (t_is_empty(t_left(t_right(tree)))) {
        fprintf(stderr, "rotate_rl: empty left tree within right tree.\n");
        exit(EXIT_FAILURE);
    }

    return rotate_l(
            t_embed_cell(t_left(tree), tree, rotate_r(t_right(tree)))
    );
}

tree_t t_balance(tree_t tree) {
    if (t_diff(tree) >= IMBALANCE_LIMIT) {
        if (t_diff(t_left(tree)) >= 0) {
            tree = rotate_r(tree);
        }
        if (t_diff(t_left(tree)) == -1) {
            tree = rotate_lr(tree);
        }
    }
    if (t_diff(tree) == -IMBALANCE_LIMIT) {
        if (t_diff(t_right(tree)) <= 0) {
            tree = rotate_l(tree);
        }
        if (t_diff(t_right(tree)) == 1) {
            tree = rotate_rl(tree);
        }
    }

    return tree;
}

void *t_root(tree_t tree) {
    return !tree ? tree : tree->data;
}

void t_free(tree_t *tree, cmp_fn cfn, free_fn ffn) {
    while (!t_is_empty(*tree)) {
        *tree = t_del_max(tree, cfn, ffn);
    }
    free(*tree);
}

void t_free_cell(cell_t *cell, free_fn ffn) {
    if ((*ffn)) {
        (*ffn)(cell->data);
    }
    free(cell);
}

void t_print(tree_t tree, int position, print_fn fn) {
    if (!t_is_empty(tree)) {
        t_print(t_right(tree), position + 1, fn);
        (*fn)(t_root(tree), position);
        t_print(t_left(tree), position + 1, fn);
    }
}
