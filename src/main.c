/* *****************************************************************************
 * Project name: Dictionary
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "dictionary.h"
#include "tests.h"

int main(int argc, const char *const *argv) {
    int status = EXIT_FAILURE;

    if (argc < 2) {
        fprintf(stderr, "Usage: ./<executable> <dictionary_file>\n");
        goto Exit;
    }

#ifdef DEBUG
    char_tree_tests(argv[1]);
    entry_tests(argv[1]);
#else
    run(argv[1]);
#endif

    status = EXIT_SUCCESS;

    Exit:
    return status;
}
