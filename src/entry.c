/* *****************************************************************************
 * Project name: Dictionary
 * File name   : entry
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "entry.h"
#include "io.h"
#include "util.h"

bool_t e_has_empty_fields(entry_t *entry) {
    return !entry || !entry->word || !entry->def
           || str_is_empty(entry->word) || str_is_empty(entry->def);
}

bool_t e_has_empty_word(entry_t *entry) {
    return !entry || !entry->word || str_is_empty(entry->word);
}

entry_t *e_build(const char *word, const char *def) {
    entry_t *result = NULL;

    result = alloc(sizeof(entry_t));
    result->word = alloc(strlen(word) + 1);
    result->def  = alloc(strlen(def) + 1);

    strcpy(result->word, word);
    strcpy(result->def, def);

    return result;
}

entry_t *e_cp(entry_t *entry) {
    return e_build(entry->word, entry->def);
}

entry_t *e_read(FILE *file) {
    char word[WORD_LEN], def[WORD_LEN];

    entry_t *result  = NULL;
    size_t  word_len = read_line(word, WORD_LEN, file),
            def_len  = read_line(def, WORD_LEN, file);

    if (word_len && def_len) {
        result = e_build(word, def);
    }

    return result;
}

int e_cmp(entry_t *l, entry_t *r) {
    if (!l && !r) {
        return 0;
    }

    if (!l || !r) {
        return l ? 1 : -1;
    }

    return strcmp(l->word, r->word);
}

void e_free(entry_t *entry) {
    if (entry && entry->word && entry->def) {
        free(entry->word);
        free(entry->def);
        free(entry);
    }
}
