/* *****************************************************************************
 * Project name: Dictionary
 * File name   : io
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#ifndef _DICTIONARY_IO_H_
#define _DICTIONARY_IO_H_

#include <stdio.h>

// ========================== CONSTANTS AND MACROS ========================== //
#define CLEAR_SCREEN   "\e[1;1H\e[2J"

// =============================== STRUCTURES =============================== //


// ============================== DECLARATIONS ============================== //
char *input_line(const char *prompt_msg);
double input_number(const char *prompt_msg);

size_t read_line(char *dst, size_t len, FILE *file);

void print_header(const char *title);

#endif // _DICTIONARY_IO_H_
