/* *****************************************************************************
 * Project name: Dictionary
 * File name   : entry
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#ifndef _DICTIONARY_ENTRY_H_
#define _DICTIONARY_ENTRY_H_

#include <stdio.h>

#include "util.h"

// ========================== CONSTANTS AND MACROS ========================== //


// =============================== STRUCTURES =============================== //
typedef struct {
    char *word;
    char *def;
} entry_t;

// ============================== DECLARATIONS ============================== //
bool_t e_has_empty_fields(entry_t *entry);
bool_t e_has_empty_word(entry_t *entry);

entry_t *e_build(const char *word, const char *def);
entry_t *e_cp(entry_t *entry);
entry_t *e_read(FILE *file);

int e_cmp(entry_t *l, entry_t *r);

void e_free(entry_t *entry);

#endif //_DICTIONARY_ENTRY_H_
