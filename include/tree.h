/* *****************************************************************************
 * Project name: Dictionary
 * File name   : tree
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#ifndef _DICTIONARY_TREE_H_
#define _DICTIONARY_TREE_H_

#include "util.h"

// ========================== CONSTANTS AND MACROS ========================== //
#define EMPTY_TREE      -1
#define IMBALANCE_LIMIT 2

// =============================== STRUCTURES =============================== //
typedef struct cell {
    void *data;

    struct cell *l;
    struct cell *r;
} cell_t, *tree_t;

typedef struct {
    tree_t l;
    tree_t r;
} couple_t;

typedef int  (*cmp_fn)(void *, void *);
typedef void (*print_fn)(void *, int);

typedef void (*free_fn)(void *);
typedef void *(*cp_fn)(void *);

// ============================== DECLARATIONS ============================== //
bool_t t_contains(tree_t tree, void *data, cmp_fn cfn);
bool_t t_equal(tree_t l, tree_t r, cmp_fn cfn);
bool_t t_is_empty(tree_t tree);
bool_t t_is_leaf(tree_t tree);
bool_t t_is_sorted(tree_t tree, cmp_fn cfn);

cell_t *t_find(tree_t tree, void *data, cmp_fn cfn);

couple_t t_cut(tree_t tree, void *data, cmp_fn cfn);

int t_diff(tree_t tree);
int t_height(tree_t tree);
int t_leaf_count(tree_t tree);
int t_node_count(tree_t tree);

tree_t t_add_sorted(tree_t tree, void *data, cmp_fn cfn);
tree_t t_add_root(tree_t tree, void *data, cmp_fn cfn);
tree_t t_embed(tree_t l, void *data, tree_t r);
tree_t t_embed_cell(tree_t l, cell_t *cell, tree_t r);

tree_t t_del(tree_t *tree, void *data, cmp_fn cfn, free_fn ffn, cp_fn cp);
tree_t t_del_max(tree_t *tree, cmp_fn cfn, free_fn ffn);
tree_t t_del_root(tree_t *tree, cmp_fn cfn, free_fn ffn, cp_fn cp);

tree_t t_left(tree_t tree);
tree_t t_left_end(tree_t tree);
tree_t t_right(tree_t tree);
tree_t t_right_end(tree_t tree);

tree_t rotate_l(tree_t tree);
tree_t rotate_lr(tree_t tree);
tree_t rotate_r(tree_t tree);
tree_t rotate_rl(tree_t tree);

tree_t t_balance(tree_t tree);

void *t_root(tree_t tree);

void t_free(tree_t *tree, cmp_fn cfn, free_fn ffn);
void t_free_cell(cell_t *cell, free_fn ffn);
void t_print(tree_t tree, int position, print_fn fn);

#endif // _DICTIONARY_TREE_H_
