/* *****************************************************************************
 * Project name: Dictionary
 * File name   : util
 * Author      : Damien Nguyen
 * Date        : Wednesday, April 08, 2020
 * ****************************************************************************/

#ifndef _DICTIONARY_UTIL_H_
#define _DICTIONARY_UTIL_H_

#include <stdio.h>

// ========================== CONSTANTS AND MACROS ========================== //
#define CHAR_EOL '\n'
#define CHAR_EOW '\0'

#define EMPTY_STR ""

#define FILE_READ  "r"
#define FILE_WRITE "w"

#define WORD_LEN 1024

// =============================== STRUCTURES =============================== //
typedef enum {
    false, true
} bool_t;

// ============================== DECLARATIONS ============================== //
bool_t str_is_empty(const char *str);
void *alloc(size_t size);

#endif // _DICTIONARY_UTIL_H_
