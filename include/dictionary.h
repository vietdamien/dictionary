/* *****************************************************************************
 * Project name: Dictionary
 * File name   : dictionary
 * Author      : Damien Nguyen
 * Date        : Friday, April 10, 2020
 * ****************************************************************************/

#ifndef _DICTIONARY_DICTIONARY_H_
#define _DICTIONARY_DICTIONARY_H_

#include "entry.h"
#include "tree.h"

// ========================== CONSTANTS AND MACROS ========================== //


// =============================== STRUCTURES =============================== //


// ============================== DECLARATIONS ============================== //
bool_t d_handle(tree_t *tree, unsigned int choice);

unsigned int d_menu(void);

entry_t *d_input_entry(void);
entry_t *d_input_new_entry(void);
tree_t d_load(const char *file_path);

void d_print_tree(tree_t *tree);
void d_print_list(tree_t *tree);
void d_add(tree_t *tree);
void d_del(tree_t *tree);
void d_find(tree_t *tree);
void d_count(tree_t *tree);
void d_check_sort(tree_t *tree);
void d_clear(tree_t *tree);
void d_save(tree_t *tree);

void run(const char *file_path);

#endif // _DICTIONARY_DICTIONARY_H_
