/* *****************************************************************************
 * Project name: Dictionary
 * File name   : tests
 * Author      : Damien Nguyen
 * Date        : Friday, April 10, 2020
 * ****************************************************************************/

#ifndef _DICTIONARY_TESTS_H_
#define _DICTIONARY_TESTS_H_

// ========================== CONSTANTS AND MACROS ========================== //
// #define DEBUG

// =============================== STRUCTURES =============================== //


// ============================== DECLARATIONS ============================== //
void entry_tests(const char *file_path);
void str_tree_tests(const char *file_path);

#endif // _DICTIONARY_TESTS_H_
